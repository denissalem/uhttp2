#include "uhttp2.h"

uhttp2_context * uhttp2_create_context() {
	uhttp2_context * context = malloc(sizeof(uhttp2_context));
	if (context == NULL) {
		return NULL;
	}
	memset(context, 0, sizeof(uhttp2_context));
}

void uhttp2_free_context(uhttp2_context * context) {
	if (context == NULL) {
		return;
	}
	for (int i=0; i < uHTTP2_PENDING_FRAMES_BUFFER_SIZE; i++) {
		free(context->pending_ingoing_frames[i].payload);
		free(context->pending_outgoing_frames[i].payload);
	}
	free(context);
}

unsigned int uhttp2_acknowledge_magic_sequence(unsigned char * data, unsigned int size) {
  	if (size < uHTTP2_MAGIC_SEQUENCE_LEN) {
		return 0;
	}
	for (int i = 0; i < uHTTP2_MAGIC_SEQUENCE_LEN; i++) {
		if (data[i] != uHTTP2_MAGIC_SEQUENCE[i])
			return 0;
	}
	return 1;
}

unsigned int uhttp2_setting_frame_type_4(uhttp2_frame * frame, unsigned char flag, uint16_t identifier, uint32_t value) {
	memset(frame, 0, sizeof(uhttp2_frame));
	frame->type = 0x04;
	frame->stream_identifier = 0;
	frame->flag = flag;
	if (flag == uHTTP2_FLAG_SETTING_FRAME_ACK) {
		frame->length[2] = 0;
		frame->payload = 0;
	}	
	else {
		frame->length[2] = 6;
		frame->payload = malloc(6);
		if (frame->payload == NULL) {
			return uHTTP2_CANNOT_ALLOCATE_FRAME;
		}
	}
	return uHTTP2_NO_ERROR;
}

unsigned int uhttp2_setting_frame(unsigned char * data, unsigned int size, uhttp2_frame * frame) {
	if (size < 9) {
		return uHTTP2_INVALID_FRAME_SIZE; 
	}
	for (int i = 0; i < 3; i++) {
		if (i < 3) {
			frame->length[i] = data[i];
		}
	}
	frame->type = data[3];
	frame->flag = data[4];
	frame->stream_identifier = (uint32_t) data[5] << 24 | data[6] << 16 | data[7] << 8 | data[8];
	size_t payload_length = uHTTP2_FRAME_LENGTH(frame->length);
	frame->payload = malloc(payload_length);
	if (frame->payload == NULL) {
		return uHTTP2_CANNOT_ALLOCATE_FRAME;
	}
	memcpy(frame->payload, data+uHTTP2_FRAME_HEADER_SIZE, payload_length);
	return uHTTP2_NO_ERROR;
}

unsigned int uhttp2_update_endpoint_settings(uhttp2_context * context, uhttp2_frame * frame) {
	int i = 0;
	uint16_t identifier = 0;
	uint32_t value = 0;
	unsigned int frame_length = uHTTP2_FRAME_LENGTH(frame->length);
	if (	frame_length % 48 != 0) {
		return uHTTP2_PROTOCOL_ERROR_FRAME_SIZE_ERROR;
	}
	while (i < frame_length) {
		identifier = (uint16_t) frame->payload[i] << 8 | frame->payload[i+1];
		value = (uint32_t) frame->payload[i+2] << 24 | frame->payload[i+3] << 16 | frame->payload[i+4] << 8 | frame->payload[i+5];
		i += 6;
		printf("%d %d\n", identifier, value);
		switch (identifier) {
			case uHTTP2_SETTINGS_HEADER_TABLE_SIZE:
				context->header_table_size = value;
				break;

			case uHTTP2_SETTINGS_ENABLE_PUSH:
				if (value != 0 | value != 1) {
					return uHTTP2_PROTOCOL_ERROR_PROTOCOL_ERROR;
				}
				context->enable_push = value;
				break;

			case uHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS:
				context->max_concurrent_streams = value;
				break;

			case uHTTP2_SETTINGS_INITIAL_WINDOW_SIZE:
				if (value > 0x7fffffff) {
					return uHTTP2_PROTOCOL_ERROR_FLOW_CONTROL_ERROR;
				}
				context->initial_window_size = value;
				break;

			case uHTTP2_SETTINGS_MAX_FRAME_SIZE:
				if (value < 0x4000 | value > 0xffffff ) {
					return uHTTP2_PROTOCOL_ERROR_PROTOCOL_ERROR;
				}
				context->max_frame_size = value;
				break;

			case uHTTP2_SETTINGS_MAX_HEADER_LIST_SIZE:
				context->max_header_list_size = value;
				break;

			default:
				break;
		}
	}
		
	return uHTTP2_NO_ERROR;
}

unsigned int uhttp2_process_payload(uhttp2_context * context, uhttp2_frame * frame) {
	unsigned int ret;
	switch(frame->type) {
		case uHTTP2_FRAME_TYPE_SETTING:
			context->initial_setting_frame_acknowledged = 1;
			ret = uhttp2_update_endpoint_settings(context, &(context->pending_ingoing_frames[context->pending_ingoing_frames_index]));
			break;

		default:
			// TODO
			// Handle unknown frame
			break;
	}
	if (ret != uHTTP2_NO_ERROR) {
		// TODO
		// Handle invalid frame
	}
	context->pending_ingoing_frames_index++;
}

// TODO
// check that pending frames stacks aren't greater than uHTTP2_PENDING_FRAMES_BUFFER_SIZE
unsigned int uhttp2_process_ingoing(uhttp2_context * context, unsigned char * data, unsigned int size) {
	int ret;
	if (context->magic_sequence_acknowledged) {
		ret = uhttp2_setting_frame(data, size, &(context->pending_ingoing_frames[context->pending_ingoing_frames_index]));
		if (ret == uHTTP2_NO_ERROR) {
			ret = uhttp2_process_payload(context, &(context->pending_ingoing_frames[context->pending_ingoing_frames_index]));
		}
		else {
			// TODO
	  		// process ret
			// send FRAME_SIZE_ERROR if uHTTP2_INVALID_FRAME_SIZE
			// Treat as connection error if frame is:
			// HEADER, PUSH_PROMISE, CONTINUATION, SETTINGS
			// or any frame with stream identifier set tot 0
			return ret;
		}
	}
	else {
		context->magic_sequence_acknowledged = uhttp2_acknowledge_magic_sequence(data, size);
		if (!context->magic_sequence_acknowledged) {
			// TODO
			// send goaway frame
			return uHTTP2_INVALID_PREFACE;
		}
	}
	return uHTTP2_NO_ERROR;
}

unsigned int uhttp2_process_outgoing(uhttp2_context * context, unsigned char * data, unsigned int size, void * args) {
	int ret = uHTTP2_NO_ERROR;
	if (context->process_outgoing == NULL) {
		return uHTTP2_CALLBACK_IS_NULL;
	}
	else {
		ret = context->process_outgoing(context, data, size, args);
		if (ret != uHTTP2_NO_ERROR) {
			return uHTTP2_CALLBACK_ADVERTISE_ERROR;
		}
	}
	return uHTTP2_NO_ERROR;
}

void uhttp2_printf_frame_header(uhttp2_frame * frame) {
	printf("Frame length: %d\n", (unsigned int) uHTTP2_FRAME_LENGTH(frame->length));
	printf("Type: %d, ", frame->type);
	switch(frame->type) {
		case uHTTP2_FRAME_TYPE_DATA:
			printf("DATA\n");
			break;
		case uHTTP2_FRAME_TYPE_HEADER:
			printf("HEADER\n");
			break;
		case uHTTP2_FRAME_TYPE_PRIORITY:
			printf("PRIORITY\n");
			break;
		case uHTTP2_FRAME_TYPE_RST_STREAM:
			printf("RST_STREAM\n");
			break;
		case uHTTP2_FRAME_TYPE_SETTING:
			printf("SETTING\n");
			break;
		case uHTTP2_FRAME_TYPE_PUSH_PROMISE:
			printf("PUSH_PROMISE\n");
			break;
		case uHTTP2_FRAME_TYPE_PING:
			printf("PING\n");
			break;
		case uHTTP2_FRAME_TYPE_GOAWAY:
			printf("GOAWAY\n");
			break;
		case uHTTP2_FRAME_TYPE_WINDOW_UPDATE:
			printf("WINDOW_UPDATE\n");
			break;
		case uHTTP2_FRAME_TYPE_CONTINUATION:
			printf("CONTINUATION\n");
			break;
		default:
			printf("UNKNOWN\n");
	}
	printf("Flag: %d\n", frame->flag);
	printf("Reserved: %d\n", (frame->stream_identifier & 0x80000000) >> 31);
	printf("Stream Identifier: %d\n", frame->stream_identifier & 0x7fffffff);
	printf("\n");
}
