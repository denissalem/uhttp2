#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define uHTTP2_VERSION_MAJOR @uHTTP2_VERSION_MAJOR@
#define uHTTP2_VERSION_MINOR @uHTTP2_VERSION_MINOR@

/* HTTP2 constants */
#define uHTTP2_MAGIC_SEQUENCE     "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"
#define uHTTP2_MAGIC_SEQUENCE_LEN 24
#define uHTTP2_PROTO_ID           "h2"
#define uHTTP2_PROTO_ID_LEN       2
#define uHTTP2_PROTO_CLEAR_ID     "h2c"
#define uHTTP2_PROTO_CLEAR_LEN    3
#define uHTTP2_FRAME_HEADER_SIZE  9
#define uHTTP2_SETTINGS_HEADER_TABLE_SIZE      0x01
#define uHTTP2_SETTINGS_ENABLE_PUSH            0x02
#define uHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS 0x03
#define uHTTP2_SETTINGS_INITIAL_WINDOW_SIZE    0x04
#define uHTTP2_SETTINGS_MAX_FRAME_SIZE         0x05
#define uHTTP2_SETTINGS_MAX_HEADER_LIST_SIZE   0x06
#define uHTTP2_FRAME_TYPE_DATA          0x00
#define uHTTP2_FRAME_TYPE_HEADER        0x01
#define uHTTP2_FRAME_TYPE_PRIORITY      0x02
#define uHTTP2_FRAME_TYPE_RST_STREAM    0x03
#define uHTTP2_FRAME_TYPE_SETTING       0x04
#define uHTTP2_FRAME_TYPE_PUSH_PROMISE  0x05
#define uHTTP2_FRAME_TYPE_PING          0x06
#define uHTTP2_FRAME_TYPE_GOAWAY        0x07
#define uHTTP2_FRAME_TYPE_WINDOW_UPDATE 0x08
#define uHTTP2_FRAME_TYPE_CONTINUATION  0x09
#define uHTTP2_PROTOCOL_ERROR_NO_ERROR            0x00
#define uHTTP2_PROTOCOL_ERROR_PROTOCOL_ERROR      0x01
#define uHTTP2_PROTOCOL_ERROR_INTERNAL_ERROR      0x02
#define uHTTP2_PROTOCOL_ERROR_FLOW_CONTROL_ERROR  0x03
#define uHTTP2_PROTOCOL_ERROR_SETTINGS_TIMEOUT    0x04
#define uHTTP2_PROTOCOL_ERROR_STREAM_CLOSED       0x05
#define uHTTP2_PROTOCOL_ERROR_FRAME_SIZE_ERROR    0x06
#define uHTTP2_PROTOCOL_ERROR_REFUSED_STREAM      0x07
#define uHTTP2_PROTOCOL_ERROR_CANCEL              0x08
#define uHTTP2_PROTOCOL_ERROR_COMPRESSION_ERROR   0x09
#define uHTTP2_PROTOCOL_ERROR_CONNECT_ERROR       0x0A
#define uHTTP2_PROTOCOL_ERROR_ENHANCE_YOUR_CALM   0x0B
#define uHTTP2_PROTOCOL_ERROR_INADEQUATE_SECURITY 0x0C
#define uHTTP2_PROTOCOL_ERROR_HTTP_1_1_REQUIRED   0x0D


/* HTTP2 Flags */
#define uHTTP2_FLAG_NONE 0
#define uHTTP2_FLAG_SETTING_FRAME_ACK 1

/* uHTTP2 internal error codes */
#define uHTTP2_NO_ERROR                 0x0000
#define uHTTP2_INVALID_PREFACE          0x0001
#define uHTTP2_CANNOT_ALLOCATE_FRAME    0x0002
#define uHTTP2_CALLBACK_IS_NULL         0x0003
#define uHTTP2_CALLBACK_ADVERTISE_ERROR 0x0004
#define uHTTP2_INVALID_FRAME_SIZE       0x0005

/* uHTTP2 internal constants */
#define uHTTP2_PENDING_FRAMES_BUFFER_SIZE 128

/* uHTTP2 useful macros */

#define uHTTP2_FRAME_LENGTH(buf) ((size_t) buf[0] << 16 | buf[1] << 8 | buf[2])

/* uHTTP2 data structures forward declaration */
typedef struct _uhttp2_context_ uhttp2_context;
typedef struct _uhttp2_frame_   uhttp2_frame;

/* Callback pointers */
typedef int (*cb_process_outgoing)(uhttp2_context * context, unsigned char * data, unsigned int size, void * args);

/* uHTTP2 data structures */
typedef struct _uhttp2_frame_ {
	unsigned char length[3];
	unsigned char type;
	unsigned char flag;
	uint32_t stream_identifier;
	unsigned char * payload;
} uhttp2_frame;

typedef struct _uhttp2_context_ {
	unsigned int magic_sequence_acknowledged;
	unsigned int initial_setting_frame_acknowledged;
	uhttp2_frame pending_ingoing_frames[uHTTP2_PENDING_FRAMES_BUFFER_SIZE];
	uhttp2_frame pending_outgoing_frames[uHTTP2_PENDING_FRAMES_BUFFER_SIZE];
	unsigned int pending_ingoing_frames_index;
	unsigned int pending_outgoing_frames_index;
	uint32_t header_table_size; // TODO initial value: 4096
	uint32_t enable_push;       // TODO initial value: 1
	uint32_t max_concurrent_streams;
	uint32_t initial_window_size; // TODO initial value: 65535
	uint32_t max_frame_size; // TODO initial value: 16384
	uint32_t max_header_list_size; // TODO initial value: 16384
	
	cb_process_outgoing process_outgoing;
} uhttp2_context;

/* uHTTP2 API */
uhttp2_context * uhttp2_create_context();
void uhttp2_free_context(uhttp2_context * context);
unsigned int uhttp2_acknowledge_magic_sequence(unsigned char * data, unsigned int size);
unsigned int uhttp2_setting_frame_type_4(uhttp2_frame * frame, unsigned char flag, uint16_t identifier, uint32_t value);
unsigned int uhttp2_setting_frame(unsigned char * data, unsigned int size, uhttp2_frame * frame);
unsigned int uhttp2_update_endpoint_settings(uhttp2_context * context, uhttp2_frame * frame);
unsigned int uhttp2_process_payload(uhttp2_context * context, uhttp2_frame * frame);
unsigned int uhttp2_process_ingoing(uhttp2_context * context, unsigned char * data, unsigned int size);
unsigned int uhttp2_process_outgoing(uhttp2_context * context, unsigned char * data, unsigned int size, void * args);
void uhttp2_printf_frame_header(uhttp2_frame * frame);

