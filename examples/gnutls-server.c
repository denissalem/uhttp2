/* This example code is placed in the public domain. */

// $ certtool --generate-privkey --bits 4096 --pkcs-cipher aes-256 --outfile key.pem --rsa --password="replace_me" --hash=SHA512
// $ certtool --generate-self-signed --load-privkey key.pem --outfile cert.pem

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <assert.h>
#include "uhttp2.h"

#define KEYFILE "key.pem"
#define CERTFILE "cert.pem"
#define CAFILE "/etc/ssl/certs/ca-certificates.crt"

#define CHECK(x) assert((x)>=0)

#define MAX_BUF 1024
#define PORT 4433

int main(void)
{
        int listen_sd;
        int sd, ret;
        gnutls_certificate_credentials_t x509_cred;
        gnutls_priority_t priority_cache;
        struct sockaddr_in sa_serv;
        struct sockaddr_in sa_cli;
        socklen_t client_len;
        char topbuf[512];
        gnutls_session_t session;
        char buffer[MAX_BUF + 1];
        int optval = 1;

        /* for backwards compatibility with gnutls < 3.3.0 */
        CHECK(gnutls_global_init());
        CHECK(gnutls_certificate_allocate_credentials(&x509_cred));
        CHECK(gnutls_certificate_set_x509_trust_file(x509_cred, CAFILE, GNUTLS_X509_FMT_PEM));
        CHECK(gnutls_certificate_set_x509_key_file2(x509_cred, CERTFILE, KEYFILE, GNUTLS_X509_FMT_PEM, "replace_me", 0));
        CHECK(gnutls_priority_init(&priority_cache, NULL, NULL));
        gnutls_certificate_set_known_dh_params(x509_cred, GNUTLS_SEC_PARAM_MEDIUM);

        /* Socket operations */
        listen_sd = socket(AF_INET, SOCK_STREAM, 0);

        memset(&sa_serv, '\0', sizeof(sa_serv));
        sa_serv.sin_family = AF_INET;
        sa_serv.sin_addr.s_addr = INADDR_ANY;
        sa_serv.sin_port = htons(PORT); /* Server Port number */

        setsockopt(listen_sd, SOL_SOCKET, SO_REUSEADDR, (void *) &optval, sizeof(int));

        bind(listen_sd, (struct sockaddr *) &sa_serv, sizeof(sa_serv));

        listen(listen_sd, 1024);

        printf("Server ready. Listening to port '%d'.\n\n", PORT);

        client_len = sizeof(sa_cli);

	gnutls_datum_t protos[1];
	protos[0].data = uHTTP2_PROTO_ID;
	protos[0].size = uHTTP2_PROTO_ID_LEN;

	/* uHTTP2 */
	uhttp2_context * http2_context = NULL;
	uhttp2_create_context();
	CHECK(http2_context != NULL);
	
        for (;;) {
                CHECK(gnutls_init(&session, GNUTLS_SERVER));

		ret = gnutls_alpn_set_protocols(session, protos, 1, 0);
		if (ret < 0) {
			gnutls_perror(ret);
			exit(1);
		}

                CHECK(gnutls_priority_set(session, priority_cache));
                CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, x509_cred));

                gnutls_certificate_server_set_request(session, GNUTLS_CERT_IGNORE); /* this is a code example, we don't care about certificate validation */
                gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

		gnutls_datum_t proto = {0};
                sd = accept(listen_sd, (struct sockaddr *) &sa_cli, &client_len);

                printf("- connection from %s, port %d\n", inet_ntop(AF_INET, &sa_cli.sin_addr, topbuf, sizeof(topbuf)), ntohs(sa_cli.sin_port));

                gnutls_transport_set_int(session, sd);

                do {
                        ret = gnutls_handshake(session);
                }
                while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

                if (ret < 0) {
                        close(sd);
                        gnutls_deinit(session);
                        fprintf(stderr,
                                "*** Handshake has failed (%s)\n\n",
                                gnutls_strerror(ret));
                        continue;
                }
                printf("- Handshake was completed\n");

                /* see the Getting peer's information example */
                /* print_info(session); */
		// TODO
		// Check if selected protoc is really h2
		http2_context = uhttp2_create_context();
		CHECK(http2_context != NULL);

                for (;;) {
                        ret = gnutls_record_recv(session, buffer, MAX_BUF);

                        if (ret == 0) {
                                printf
                                    ("\n- Peer has closed the GnuTLS connection\n");
                                break;
                        } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
                                fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));

                        } else if (ret < 0) {
                                fprintf(stderr, "\n*** Received corrupted data(%d). Closing the connection.\n\n", ret);
                                break;
                        } else if (ret > 0) {
				uhttp2_process_ingoing(http2_context, buffer, ret);
                                //gnutls_record_send(session, buffer, ret);
                        }
                }
                printf("\n");
                /* do not wait for the peer to close the connection. */
                CHECK(gnutls_bye(session, GNUTLS_SHUT_WR));
		uhttp2_free_context(http2_context);

                close(sd);
                gnutls_deinit(session);
        }
        close(listen_sd);

        gnutls_certificate_free_credentials(x509_cred);
        gnutls_priority_deinit(priority_cache);

        gnutls_global_deinit();

        return 0;
}
